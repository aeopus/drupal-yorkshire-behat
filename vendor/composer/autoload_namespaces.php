<?php

// autoload_namespaces.php generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'WebDriver' => array($vendorDir . '/instaclick/php-webdriver/lib'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Validator\\' => array($vendorDir . '/symfony/validator'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Templating\\' => array($vendorDir . '/symfony/templating'),
    'Symfony\\Component\\Stopwatch\\' => array($vendorDir . '/symfony/stopwatch'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\PropertyAccess\\' => array($vendorDir . '/symfony/property-access'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\Intl\\' => array($vendorDir . '/symfony/intl'),
    'Symfony\\Component\\Icu\\' => array($vendorDir . '/symfony/icu'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Form\\' => array($vendorDir . '/symfony/form'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\DependencyInjection\\' => array($vendorDir . '/symfony/dependency-injection'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\Config\\' => array($vendorDir . '/symfony/config'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Symfony\\Bundle\\FrameworkBundle\\' => array($vendorDir . '/symfony/framework-bundle'),
    'Selenium' => array($vendorDir . '/alexandresalome/php-selenium/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Goutte' => array($vendorDir . '/fabpot/goutte'),
    'Drupal\\Exception' => array($vendorDir . '/drupal/drupal-extension/src'),
    'Drupal\\DrupalExtension' => array($vendorDir . '/drupal/drupal-extension/src'),
    'Drupal\\Drupal' => array($vendorDir . '/drupal/drupal-extension/src'),
    'Drupal\\Driver' => array($vendorDir . '/drupal/drupal-extension/src'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
    'Buzz' => array($vendorDir . '/kriswallsmith/buzz/lib'),
    'Behat\\Symfony2Extension' => array($vendorDir . '/behat/symfony2-extension/src'),
    'Behat\\SahiClient' => array($vendorDir . '/behat/sahi-client/src'),
    'Behat\\Mink\\Driver' => array($vendorDir . '/behat/mink-zombie-driver/src', $vendorDir . '/behat/mink-selenium2-driver/src', $vendorDir . '/behat/mink-browserkit-driver/src', $vendorDir . '/behat/mink-goutte-driver/src', $vendorDir . '/behat/mink-selenium-driver/src', $vendorDir . '/behat/mink-sahi-driver/src'),
    'Behat\\MinkExtension' => array($vendorDir . '/behat/mink-extension/src'),
    'Behat\\Mink' => array($vendorDir . '/behat/mink/src'),
    'Behat\\Gherkin' => array($vendorDir . '/behat/gherkin/src'),
    'Behat\\Behat' => array($vendorDir . '/behat/behat/src'),
);
